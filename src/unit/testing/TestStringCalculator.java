package unit.testing;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.omg.CORBA.PUBLIC_MEMBER;

import mx.baz.StringCalculator;

class TestStringCalculator {

	@Test
	void testPositiveNumbers() {
		StringCalculator stCal = new StringCalculator();	
		System.out.println("testPositiveNumbers:");
		assertEquals("6.0", stCal.add("1,2,3"));
		assertEquals("1.0", stCal.add("1"));
	}
	

	@Test
	void testEmptyString() {
		System.out.println("testEmptyString:");
		StringCalculator stCal = new StringCalculator();		
		assertEquals("0.0", stCal.add(" "));
	}	
	
	@Test
	void testNegativeNumbers() {
		System.out.println("testNegativeNumbers:");
		StringCalculator stCal = new StringCalculator();		
		assertEquals("-3.0", stCal.add("-1,1,-3"));
	}
	
	@Test
	void testDecimalNumbers() {
		System.out.println("testDecimalNumbers:");
		StringCalculator stCal = new StringCalculator();		
		assertEquals("-3.4", stCal.add("-1.4,1.6,-3.6"));
	}
	
	@Test
	void testNewSeparator() {
		StringCalculator stCal = new StringCalculator();		
		System.out.println("testNewSeparator:");
		assertEquals("-3.4", stCal.add("-1.4\n1.6,-3.6"));
		assertEquals("Number expected but \\n found at position 6", stCal.add("175.2,\n35"));
	}
	@Test
	void testLastPosition() {
		StringCalculator stCal = new StringCalculator();		
		System.out.println("FaultLastPosition:");
		assertEquals("Number expected but EOF found", stCal.add("1,3,"));
		
	}
	

}
