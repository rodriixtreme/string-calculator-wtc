package mx.baz;

import java.lang.reflect.Array;
import java.util.StringTokenizer;

public class StringCalculator {

	public String add(String number) {

		number = number.trim();
		String error = "";
		Double values = 0d;
		String result = "";
		if (!number.equals("")) {
			number = number.replaceAll("\n", ",");
			String lastPosition = number.substring((number.length() - 1), number.length());
			if (lastPosition.equals(",")) {
				error = "Number expected but EOF found";
				result = error;
			}
		}
		int positionError = number.indexOf(",,");
		if (positionError == -1 && error.equals("")  ) {
			StringTokenizer st = new StringTokenizer(number, ",");

			while (st.hasMoreTokens()) {
				values = values + Double.parseDouble(st.nextToken());
			}
				
			System.out.println(values);
			result = values.toString();
		} 
		if (positionError != -1 && error.equals("") ) {
			error = "Number expected but \\n found at position " + ++positionError;
			result = error;
		}
		System.out.println(error);
		return result;
	}
}
